﻿using UnityEngine;
using System.Collections;

public class CombatCamera : MonoBehaviour {

	public float minMapXDistance =-25;
	public float maxMapXDistance = 25;
	public float mapLimitOffset = 5;
	public float positionDampTime = 0.2f;
	public float rotationDampTime = 0.8f;
	public bool dinamicViewAngle = true;
	public float currentViewAngleDeegre= 35;
	public float minViewAngle = 35f;
	public float maxViewAngle = 70f;
	public float minTargetDistanceForZoomOut = 3.5f;
	public float maxTargetDistanceForZoomOut = 24f;
	public float maxRotationAngle = 3f;

	public float yOffset = -1f;

	private Vector3 moveVelocity;
	private float angleVelocity;
	private Vector3 targetPosition;
	private float targetAngle = 0;
	private Camera camera;

	public Transform[] cameraTargets;

	// Use this for initialization
	void Awake () {
		camera = GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {

		targetPosition = transform.position;
		float sumPosX = 0, sumPosY = 0;
	 
		for (int i = 0; i < cameraTargets.Length; i++) {
			sumPosX += cameraTargets [i].position.x;
			sumPosY += cameraTargets [i].position.y;
		}
		targetPosition.x = sumPosX / cameraTargets.Length;
		targetPosition.y = (sumPosY / cameraTargets.Length) + yOffset;

		targetPosition.x = Mathf.Clamp (targetPosition.x, minMapXDistance + mapLimitOffset, maxMapXDistance - mapLimitOffset);
		//camera.pixelWidth

		float maxTargetDistance = 0;
		for (int i = 0; i < cameraTargets.Length; i++) 
			maxTargetDistance = Mathf.Max(maxTargetDistance, Mathf.Abs(targetPosition.x-cameraTargets[i].position.x));

		maxTargetDistance = Mathf.Max(maxTargetDistance, minTargetDistanceForZoomOut);

		if (dinamicViewAngle) { 
			float angleAux = (maxTargetDistance - minTargetDistanceForZoomOut) / maxTargetDistanceForZoomOut;  
			currentViewAngleDeegre = Mathf.Lerp (minViewAngle, maxViewAngle, angleAux);
		} else
			currentViewAngleDeegre = (minViewAngle + maxViewAngle) / 2f;
		

		targetPosition.z = -maxTargetDistance / Mathf.Atan(0.034907f*currentViewAngleDeegre/2);

		transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref moveVelocity, positionDampTime);

		targetAngle = (targetPosition.x - transform.position.x) * maxRotationAngle;
		Vector3 angles = transform.rotation.eulerAngles;
		angles.y = Mathf.SmoothDampAngle( angles.y, targetAngle, ref angleVelocity, rotationDampTime);
		transform.rotation = Quaternion.Euler(angles); 
	}


}
