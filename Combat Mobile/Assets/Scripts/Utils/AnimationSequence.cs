﻿using UnityEngine;
using System.Collections;

public class AnimationSequence : MonoBehaviour {

	Animator anim;
	private string[] variableNames;

	[Tooltip("{Crouch, Run, AttackDirection, AttackType, Grounded, Block, HitType}")]
	public int[] names;
	public float[] values;
	public float[] times;

	private int currentState = 0;
	private float currentTime;

	// Use this for initialization
	void Awake () {
		anim = GetComponent<Animator> ();
		variableNames = new string[]{"Crouch","Run","AttackDirection","AttackType","Grounded", "Block", "HitType"};

		currentTime = times [currentState];
		setAnimation (names [currentState], values [currentState], values [currentState]);
	}
	
	// Update is called once per frame
	void Update () {
		currentTime -= Time.deltaTime;
		if (currentTime <= 0) {
			currentState++;
			currentState %= times.Length;

			currentTime = times [currentState];
			setAnimation (names [currentState], values [currentState], values [currentState]);

		}
	}

	private void setAnimation(int name, float value, float time){
		if (name < 0)
			return;
		else if (name >= 0 && name <= 3)
			anim.SetFloat (variableNames [name], value);
		else if (name >= 4 && name <= 5)
			anim.SetBool (variableNames [name], (value >= 1));
		else 
			anim.SetInteger (variableNames [name], (int) value);
	}
}
