using UnityEngine;

// Demo class for the Language control
[System.Serializable]
public class Language {

	public string current;
	public string lastLang;

	public void SetLanguage(string value){

//		Debug.Log ("Set Game Language to :: "+value);

		lastLang = current;
		current = value;

		if (lastLang == null)
			lastLang = current;
	}
}