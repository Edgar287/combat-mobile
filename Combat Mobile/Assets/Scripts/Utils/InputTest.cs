﻿using UnityEngine;
using System.Collections;

public class InputTest : MonoBehaviour {

	Animator animator;


	void Awake () {
		animator = GetComponent<Animator> ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown (KeyCode.A)) {

			Debug.Log ("GetKeyDown A AttackType");

			animator.SetFloat ("AttackType", Random.Range (0f, 2f));
		}

	}
}
