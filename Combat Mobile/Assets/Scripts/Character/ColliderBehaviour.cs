﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))] // Must be Kinematic
[RequireComponent(typeof(BoxCollider2D))]

public class ColliderBehaviour : MonoBehaviour {

	public Transform targetBodyPart; // The transform which the collider will follow
	private BoxCollider2D _collider;
	public float damage; // Damage that inflicts
	CharacterController otherChar;
	CharacterController myChar;
	private bool isColliding;

	// Use this for initialization
	void Start () {
		_collider = GetComponent<BoxCollider2D> () as BoxCollider2D;
		MoveToTargetBeforeEnableCollider (); // Avoid first frame collision
		myChar = transform.root.GetComponent<CharacterController> () as CharacterController; // Char who owns this collider
	}
	
	// Update is called once per frame
	void Update () {
		_collider.transform.position = targetBodyPart.position; // Follow the body part
	}

	private void MoveToTargetBeforeEnableCollider(){  // Avoid collissions on first frame
		_collider.enabled = false;
		_collider.transform.position = targetBodyPart.position;
		_collider.enabled = true;
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (!isColliding) {
			isColliding = true;
//			print (gameObject.name + " collissioned with " + other.gameObject.name);
//			print ("ENTER");

			if (other.tag == "Character") {
				otherChar = other.GetComponent<CharacterController> () as CharacterController;
			} else {
				otherChar = null;
			}

			// Only for body colliders of villain characters
			if(otherChar != null)
			if (other.tag != "AgressiveCollider" && otherChar.Data.IsHero != myChar.Data.IsHero) {

				otherChar.SubstractHealth (damage);

//				print (gameObject.name + " collissioned with " + other.gameObject.name);
//				print (otherChar.CurrentHealth);
			}
		}
	}
	void OnTriggerExit2D(Collider2D other) {
//		print (gameObject.name + " stop collisioning with " + other.gameObject.name);
//		print ("EXIT");
		isColliding = false;
	}
	public float Damage {
		get {
			return damage;
		}
	}
}
