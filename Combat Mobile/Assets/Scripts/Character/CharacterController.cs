﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour {

//	public static CharacterController instance = null; //Static instance of GameManager which allows it to be accessed by any other script.

	private Rigidbody2D charRB;
	private LifeBarController lifeBar;
	private PlayerData data;
	private AnimationController animationCtrl;

	void Awake () {
	}

	// Use this for initialization
	void Start () {
		charRB = GetComponent<Rigidbody2D> () as Rigidbody2D;
		animationCtrl = GetComponent<AnimationController>() as AnimationController;
		CombatSceneController sceneController = GameManager.Instance.sceneController as CombatSceneController;
		lifeBar = sceneController.uiController.lifeBarCtrl;
		data = GetComponent<PlayerData> () as PlayerData;
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetKey (KeyCode.Space)) {
//			Move(-1);
//		}
	}
		
	public bool Jump(int direction = 0){ // -1 for left, 1 for right, 0 default for basic jump up
		if(data.IsOnFloor){ // Avoid double jump
			charRB.velocity = Vector2.zero;
			charRB.AddForce ((Vector2.right * direction + Vector2.up) * data.JumpForce , ForceMode2D.Impulse);
			animationCtrl.SetGrounded (false);
			return true;
		}
		return false;
	}

	public void Move(int direction, int multi = 1){

		if (data.IsOnFloor) { // Avoid double jump

			Debug.Log ("IsOnFloor");

			charRB.AddForce (Vector2.right * direction * data.MovementSpeed * multi, ForceMode2D.Force);
			charRB.velocity = Vector2.ClampMagnitude (charRB.velocity, data.movementSpeed);

		} else if (data.AirControl) {

			Debug.Log ("AirControl");

			charRB.AddForce (Vector2.right * direction * data.MovementSpeed * multi, ForceMode2D.Force);
			charRB.velocity = Vector2.ClampMagnitude (charRB.velocity, data.movementSpeed);

		} else {

			Debug.Log ("NO IsOnFloor + NO AirControl");

		}
	}

	public void Attack(float val){
		
		StartCoroutine (animationCtrl.SetAttack (val));
	}

	public void SubstractHealth(float damage){
		data.CurrentLife -= damage;
		CheckIfDead ();

		//Tell UIController
		lifeBar.SetHealth (data.CurrentLife, data.IsHero);
	}

	private void CheckIfDead(){
		data.IsDead = data.CurrentLife <= 0;
		if (data.IsDead)
			data.CurrentLife = 0;
	}

	public PlayerData Data {
		get {
			return data;
		}
	}

	// COLLISION
	// Groundcheck Collider
	void OnTriggerEnter2D(Collider2D col)
	{
		if (!animationCtrl.Animator.GetBool ("Grounded")) {
			animationCtrl.SetGrounded (true);
		}

		data.IsOnFloor = true;
	}

	void OnTriggerExit2D(Collider2D col)
	{
		data.IsOnFloor = false;
	}

}
