﻿using UnityEngine;
using System.Collections;

public class CharSelectionSceneController : SceneController {

	public CharSelectionUIController uiController;

	void Start () {

		Debug.Log ("Start :: " + GM.GetGameScene());

		// Active the UI
		uiController.ActiveUI ();
	}

	protected override void OnStateChangeHandler ()
	{
		base.OnStateChangeHandler ();

		// Create switch statement to check scene states
		switch(GM.GetSceneState()) {

		default:
			break;
		}
	}
}
