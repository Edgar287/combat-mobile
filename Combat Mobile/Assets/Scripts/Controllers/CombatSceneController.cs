﻿using UnityEngine;
using System.Collections;

public class CombatSceneController : SceneController {

	public CombatUIController uiController;

	public CharacterController characterController;

	public InputController inputController;

	void Start () {

		Debug.Log ("Start :: " + GM.GetGameScene());

		// Active the UI
//		uiController.ActiveUI ();
	}

	protected override void OnStateChangeHandler ()
	{
		base.OnStateChangeHandler ();

		// Create switch statement to check scene states
		switch(GM.GetSceneState()) {

		default:
			break;
		}
	}

	public void Hola() {

		Debug.Log ("Hola :: ");

	}
}
