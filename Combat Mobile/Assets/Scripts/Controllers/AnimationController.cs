﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {

	GameManager GM;
	CombatSceneController sceneController;

	public AnimationState animState;

	Animator animator;


	void Awake () {

		GM = GameManager.Instance;
	}

	// Use this for initialization
	void Start () {
		
		sceneController = GM.sceneController as CombatSceneController;

		animator = GetComponentInChildren<Animator> ();
	}

	public void SetGrounded(bool b) {

		animator.SetBool ("Grounded", b);
	}

	public IEnumerator SetAttack(float val) {
		
		animator.SetFloat ("AttackType", val);

		yield return new WaitForEndOfFrame();

		animator.SetFloat ("AttackType", 0);
	}

	public Animator Animator {
		get {
			return animator;
		}
	}
}
