﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InputController : MonoBehaviour {


	GameManager GM;

	CombatSceneController sceneController;

	//The rect transform of each button
	public List<RectTransform> SpaceofButtons;

	//Record of the last combination of buttons pressed
	public List<Button> ButtonSequence;

	public bool [] ButtonsState = new bool[5];

	int _pressedCounter = 0;

	int currentTouch;


	void Awake () {

		GM = GameManager.Instance;
	}

	// Use this for initialization
	void Start () {

		sceneController = GM.sceneController as CombatSceneController;
	}
	
	// Update is called once per frame
	void Update () {
		#if UNITY_EDITOR
//		CheckClick();
		#else
		CheckTouches ();
		#endif

		// Only for UnityRemote
		CheckTouches ();
	}

	public void CheckTouches(){
		//If there's a touch or more
		if (Input.touchCount > 0) {
			
			foreach(Touch t in Input.touches){

//				Debug.Log ("phase :: " + t.phase);

				if (t.phase == TouchPhase.Began) {
					
					CheckButtonTouch (t, false);
				} 

				if (t.phase == TouchPhase.Stationary || t.phase == TouchPhase.Moved) {

					CheckButtonTouch (t, true);
				}
			}
		}

		if(Input.touches.Length == 0){

			CleanPressedStates ();
		}
	}
		
	public void CheckButtonTouch (Touch t, bool pressed){
		//Check the touck position inside the area of one of the buttons

		for(int i=0; i<SpaceofButtons.Count;i++){
			
			if (t.position.x > (SpaceofButtons [i].position.x-(SpaceofButtons[i].rect.width/2)) 
				&& t.position.x < (SpaceofButtons [i].position.x+(SpaceofButtons[i].rect.width/2) )) {
				if (t.position.y > (SpaceofButtons [i].position.y-(SpaceofButtons[i].rect.height/2)) 
					&& t.position.y < (SpaceofButtons [i].position.y+(SpaceofButtons[i].rect.height/2) )) {

					if (!pressed) {
						ButtonBegin (i);
					}else {
						currentTouch = i;
						ButtonPressed (i);
					}
				}
			}
		}

		if(pressedCounter > Input.touches.Length){

			CheckPressedCounter (currentTouch);

		}
	}

	public void CheckClick(){
		if (Input.GetMouseButtonDown(0)) {
			for(int i=0; i<SpaceofButtons.Count;i++){
				if (Input.mousePosition.x > (SpaceofButtons [i].position.x-(SpaceofButtons[i].rect.width/2))
					&& Input.mousePosition.x < (SpaceofButtons [i].position.x+(SpaceofButtons[i].rect.width/2) )) {
					if (Input.mousePosition.y > (SpaceofButtons [i].position.y-(SpaceofButtons[i].rect.height/2))
						&& Input.mousePosition.y < (SpaceofButtons [i].position.y+(SpaceofButtons[i].rect.height/2) )) {
						Debug.Log (SpaceofButtons [i].gameObject.name);

						ButtonBegin (i);
					}
				}
			}

		}
	}

	public bool CheckTouchIsActive(int indexTouch) {

		Debug.Log ("CheckTouchIsActive");

		foreach (Touch t in Input.touches) {
			if (t.position.x > (SpaceofButtons [indexTouch].position.x-(SpaceofButtons[indexTouch].rect.width/2)) 
				&& t.position.x < (SpaceofButtons [indexTouch].position.x+(SpaceofButtons[indexTouch].rect.width/2) )) {
				if (t.position.y > (SpaceofButtons [indexTouch].position.y-(SpaceofButtons[indexTouch].rect.height/2)) 
					&& t.position.y < (SpaceofButtons [indexTouch].position.y+(SpaceofButtons[indexTouch].rect.height/2) )) {
					return true;
				}
			}
		}

		return false;	
	}
		

	public void ButtonBegin (int buttonIndex) {

		Debug.Log ("ButtonBegin :: " + buttonIndex);

		int direction = 0;

		switch (buttonIndex) {

		case 0:

			Debug.Log ("Input :: STEP L");

			direction = -1;
			sceneController.characterController.Move (direction, 4);
			break;

		case 1:

			Debug.Log ("Input :: STEP R");

			direction = 1;
			sceneController.characterController.Move (direction, 4);
			break;

		case 2:

			Debug.Log ("Input :: JUMP");

			if (ButtonsState [0] == true)
				direction = -1;
			else if (ButtonsState [1] == true)
				direction = 1;

			sceneController.characterController.Jump (direction);
				
			break;

		case 3:

			Debug.Log ("Input :: ATTACK");

			sceneController.characterController.Attack (1);

			break;

		case 4:

			Debug.Log ("Input :: POWER ATTACK");

			sceneController.characterController.Attack (2);

			break;

		default:
			break;
		}
	}

	public void ButtonPressed (int buttonIndex) {

		Debug.Log ("ButtonPressed :: " + buttonIndex);

		if (ButtonsState [buttonIndex] == false) {
			ButtonsState [buttonIndex] = true;
			pressedCounter++;
		}

		int direction = 0;

		switch (buttonIndex) {

		case 0:

			Debug.Log ("Input :: MOVE L");

			direction = -1;
			sceneController.characterController.Move (direction);
			break;

		case 1:

			Debug.Log ("Input :: MOVE R");

			direction = 1;
			sceneController.characterController.Move (direction);
			break;

		case 4:

			Debug.Log ("Input :: POWER ATTACK");

			sceneController.characterController.Attack (3);

			break;

		default:
			break;
		}
	}

	public void CheckPressedCounter (int currentTouch) {

		for (int i = 0; i < ButtonsState.Length; i++) {

			if (i != currentTouch) {

				if(ButtonsState [i] == true && !sceneController.inputController.CheckTouchIsActive (i)){

					ButtonsState [i] = false;
					pressedCounter--;
				}
			}
		}
	}

	public void CleanPressedStates () {

		pressedCounter = 0;

		for (int i = 0; i < ButtonsState.Length; i++) {

			if(ButtonsState [i] == true){

				ButtonsState [i] = false;

			}
		}
	}


	public int pressedCounter {
		get {
			return _pressedCounter;
		}
		set {
			_pressedCounter = value;
		}
	}
}
