﻿using System.Collections;
using UnityEngine;

public enum MenuSection
{
	HISTORIA = 0,
	ONLINE = 1,
	TIENDA = 2,
	CREDITOS = 3,
	COMBATE,
	NULL
}

public class MenuSceneController : SceneController {

	public MenuSection menuSection {get; private set;}

	public MenuUIController uiController;

	void Start () {

		Debug.Log ("Start :: " + GM.GetGameScene());

		// Active the UI
		uiController.ActiveUI ();
	}
		
	protected override void OnStateChangeHandler ()
	{
		base.OnStateChangeHandler ();

		// Create switch statement to check scene states
		switch(GM.GetSceneState()) {

		case SceneState.MENU_SELECTED:

			// Scene change depending on the selected menu section 
			ChangeScene ();

			break;

		default:
			break;
		}
	}

	void ChangeScene () {

		switch (menuSection) {

		case MenuSection.HISTORIA:
				
			break;

		case MenuSection.ONLINE:

			break;

		case MenuSection.TIENDA:

			break;

		case MenuSection.CREDITOS:

			break;

		default:
			break;
		}


		// Allways load Combate GameScene, at the momment
		GM.LoadNextScene(GameScene.Combate);

	}

	public void SetMenuSection (MenuSection section)
	{		
		Debug.Log ("SetMenuSection :: " + section);

		menuSection = section;

	}
}
