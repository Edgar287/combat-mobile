﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour {

	protected GameManager GM;

	void Awake () {

		// Create GameManager instance
		GM = GameManager.Instance;

		// First add a callback for when the scene state changes
		GM.GameEventManager.OnStateChange += OnStateChangeHandler;

		// Configure current Scene
		GM.ConfigureSceneOnAwake (gameObject);

	}

	protected virtual void OnStateChangeHandler ()
	{
		// Create switch statement to check scene states
		switch(GM.GetSceneState()) {

		case SceneState.INIT: 
			// Add to GM as a child to acces from GM instance
			transform.parent = GM.transform;

			break;

		case SceneState.SAVE_DATA: 

			break;

		case SceneState.CHANGE:
			// Remove child from GM to erase when scene changes
			transform.parent = null;

			// Remove the scene state callback on exit scene
			GM.GameEventManager.OnStateChange -= OnStateChangeHandler;

			break;

		default:
			break;
		}
	}
}
	
// Example of runtime registration of global object
//		MyComponent myComponent = GM.RegisterComponent<MyComponent>();
//		Debug.Log(myComponent.anotherGlobalVar);
//		Debug.Log(GM.GetComponent<MyComponent>().anotherGlobalVar); // GetComponent is not recommended
//		Destroy(myComponent);
