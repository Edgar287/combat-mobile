﻿using System.Collections;
using UnityEngine;

public class IntroSceneController : SceneController {

	public IntroUIController uiController;

	void Start () {
		
		Debug.Log ("Start :: " + GM.GetGameScene());

		// Active the UI
		uiController.ActiveUI ();
	}

	protected override void OnStateChangeHandler ()
	{
		base.OnStateChangeHandler ();

		// Create switch statement to check scene states
		switch(GM.GetSceneState()) {

		case SceneState.INTRO_COMPLETE:

			// Automatically load the Menu
			ChangeScene ();

			break;


		default:
			break;
		}
	}


	void ChangeScene () {

		// Load the next Game Scene
		GM.LoadNextScene(GameScene.Menu);
	}
}