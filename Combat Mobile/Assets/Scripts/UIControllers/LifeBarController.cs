﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LifeBarController : MonoBehaviour
{

	public Image[] winStocks;

	private float player1CurrentLife;
	private float player2CurrentLife;

	//TODO get it from sceneController references
	public float player1MaxLife;
	public float player2MaxLife;

	private float player1CurrentMana;
	private float player2CurrentMana;

	//TODO get it from sceneController references
	public float player1MaxMana;
	public float player2MaxMana;

	public float player1TargetMana;
	public float player1TargetLife;
	public float player2TargetMana;
	public float player2TargetLife;

	public Slider player1LifeSlider;
	public Slider player1ManaSlider;
	public Slider player2LifeSlider;
	public Slider player2ManaSlider;

	public float lifeDampTime = 0.2f;
	public float manaDampTime = 1.8f;

	private float player1lifeSpd;
	private float player2lifeSpd;
	private float player1manaSpd;
	private float player2manaSpd;

	void Start ()
	{
		clearStocks ();
		
		player1LifeSlider.maxValue = player1MaxLife;
		player1CurrentLife = player1MaxLife;
		player1ManaSlider.maxValue = player1MaxMana;
		player2LifeSlider.maxValue = player2MaxLife;
		player2CurrentLife = player2MaxLife;
		player2ManaSlider.maxValue = player2MaxMana;
		player1CurrentMana = 0;
		player2CurrentMana = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		player1CurrentLife = Mathf.SmoothDamp (player1CurrentLife, player1TargetLife, ref player1lifeSpd, lifeDampTime);
		player1LifeSlider.value = player1CurrentLife;

		player2CurrentLife = Mathf.SmoothDamp (player2CurrentLife, player2TargetLife, ref player2lifeSpd, lifeDampTime);
		player2LifeSlider.value = player2CurrentLife;

		player1CurrentMana = Mathf.SmoothDamp (player1CurrentMana, player1TargetMana, ref player1manaSpd, manaDampTime);
		player1ManaSlider.value = player1CurrentMana;

		player2CurrentMana = Mathf.SmoothDamp (player2CurrentMana, player2TargetMana, ref player2manaSpd, manaDampTime);
		player2ManaSlider.value = player2CurrentMana;

//		print ("P1 HEALTH" + player1TargetLife);
//		print ("P2 HEALTH" + player2TargetLife);


	}

	public void clearStocks ()
	{
		for (int i = 0; i < winStocks.Length; i++)
			winStocks [i].enabled = false;
	}

	public void addStockPlayer1 ()
	{
		if (winStocks [0].enabled)
			winStocks [1].enabled = false;
		else
			winStocks [0].enabled = true;
	}

	public void addStockPlayer2 ()
	{
		if (winStocks [2].enabled)
			winStocks [3].enabled = false;
		else
			winStocks [2].enabled = true;
	}

	public void SetHealth (float health, bool isHero)
	{
		if (isHero) {
			player1TargetLife = health;
		} else {
			player2TargetLife = health;
		}
	}
}
