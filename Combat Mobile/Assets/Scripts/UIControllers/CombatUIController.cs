﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class CombatUIController : MonoBehaviour {

	GameManager GM;
	CombatSceneController sceneController;

	public LifeBarController lifeBarCtrl;
	public Button moveRight;
	public Button moveLeft;

	void Awake () {

		// Create GameManager instance
		GM = GameManager.Instance;

		// Disable script to cotrol when calls the Start() method
		this.enabled = false;
	}

	#region before Start()

	public void ActiveUI () {

		Debug.Log ("ActiveUI");

		// Call the method to set defaults params
		SetDefaults ();
	}

	// Set the initial parameters of the objects before Start() method
	void SetDefaults () {

		sceneController = GM.sceneController as CombatSceneController;

		// Enbale script, automatically calls the Start() method
		this.enabled = true;
	}

	#endregion

	// Use this for initialization
	void Start () {
		
	}
	// Update is called once per frame
	void Update () {
	}







}
