﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharSelectionUIController : MonoBehaviour {

	GameManager GM;

	public Button heroButton;
	public Button villainButton;

	void Awake () {

		// Create GameManager instance
//		GM = GameManager.Instance;

		// Disable script to cotrol when calls the Start() method
		this.enabled = false;
	}

	#region before Start()

	public void ActiveUI () {

		Debug.Log ("ActiveUI");

		// Call the method to set defaults params
		SetDefaults ();
	}

	// Set the initial parameters of the objects before Start() method
	void SetDefaults () {

		// Enbale script, automatically calls the Start() method
		this.enabled = true;
	}

	#endregion

	public void SelectChar(Button clickedBU){
		
		heroButton.image.overrideSprite = clickedBU.image.sprite;
	}

}
