﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuButton : MonoBehaviour {

	Image _bar;
	Image _butExit;
	Image _but;
	EventTrigger _trigger;
	int _index;


	void Awake () {
	
		_bar = this.gameObject.transform.GetChild (0).transform.GetComponent<Image>();
		_butExit = this.gameObject.transform.GetChild (1).transform.GetComponent<Image>();
		_but = this.gameObject.transform.GetChild (2).transform.GetComponent<Image>();
		_trigger = _but.GetComponent<EventTrigger> ();
		_trigger.enabled = false;
	}

	public Image bar {
		get {
			return _bar;
		}
	}

	public Image but {
		get {
			return _but;
		}
	}

	public Image butExit {
		get {
			return _butExit;
		}
	}

	public EventTrigger trigger {
		get {
			return _trigger;
		}
	}

	public int index {
		get {
			return _index;
		}
		set {
			_index = value;
		}
	}
}
