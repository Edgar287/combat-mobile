﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class MenuUIController : MonoBehaviour {

	GameManager GM;

	MenuSceneController sceneController;

	public Image backGround;
	public Image logoGame;

	MenuButton[] butsArray;

	int _butSelected;

	void Awake () {

		// Create GameManager instance
		GM = GameManager.Instance;

		// Disable script to cotrol when calls the Start() method
		this.enabled = false;
	}


	#region before Start()

	public void ActiveUI () {

		Debug.Log ("ActiveUI");

		// Call the method to set defaults params
		SetDefaults ();
	}


	// Set the initial parameters of the objects before Start() method
	void SetDefaults () {

		sceneController = GM.sceneController as MenuSceneController;

		// Get MenuButtons and add into the array
		butsArray = GetComponentsInChildren<MenuButton>();
		
		float alpha = 0f;
		Color color;

		for (int i = 0; i < butsArray.Length; i++) {

			butsArray [i].index = i;

			// Set button bar & button exit to alpha 0
			color = butsArray [i].bar.color;
			color.a = alpha;
			butsArray [i].bar.color = color;

			color = butsArray [i].butExit.color;
			color.a = alpha;
			butsArray [i].butExit.color = color;
		}

		// Enbale script, automatically calls the Start() method
		this.enabled = true;
	}

	#endregion


	// Use this for initialization
	void Start () {

		ShowMenuUI ();

		EnableTriggerMenuButtons (true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void ShowMenuUI() {

		float delay = 0.25f;

		backGround.DOFade (0.0f, 0.5f).SetDelay(delay).From();

		delay += 0.25f;

		logoGame.DOFade (0.0f, 0.5f).SetDelay(delay).From();
		logoGame.transform.DOMoveY (logoGame.transform.position.y - 40, 0.5f).SetDelay (delay).SetEase (Ease.OutBack).From ();
		logoGame.transform.DOScale(0.75f, 0.5f).SetDelay (delay).From ();

		delay += 0.5f;

		for (int i = 0; i < butsArray.Length; i++) {

			butsArray [i].transform.DOMoveY (butsArray [i].transform.position.y-20, 0.5f, true).SetDelay (delay).SetEase (Ease.OutCirc).From();

			if (i == butsArray.Length - 1) {
				
				butsArray [i].but.DOFade (0.0f, 0.5f).SetDelay (delay).From ();
			
			} else {

				butsArray [i].but.DOFade (0.0f, 0.5f).SetDelay (delay).From ();
			}

			delay += 0.15f;
		}
	}
		
	public void ClickMenuButton (int index) {

		EnableTriggerMenuButtons (false);

		sceneController.SetMenuSection ((MenuSection)index);

		AnimateMenu (index);
	}

	void EnableTriggerMenuButtons(bool enable){

		for (int i = 0; i < butsArray.Length; i++) {

			butsArray [i].trigger.enabled = enable;
		}
	}

	void AnimateMenu(int index){

		for (int i = 0; i < butsArray.Length; i++) {

			// Hide rest of Menu Buttons
			if (i != index) {
			
				butsArray [i].but.DOFade (0.0f, 0.1f).SetEase (Ease.Linear);
			
			} else {

				Color color = butsArray [i].butExit.color;
				color.a = 1.0f;
				butsArray [i].butExit.color = color;

				butsArray [i].but.DOFade (0.0f, 0.3f).SetEase (Ease.Linear).OnComplete(ChangeMenuState);
				butsArray [i].bar.DOFade (1.0f, 0.25f).SetEase (Ease.Linear);
				butsArray [i].bar.transform.DOScaleX (0.5f, 0.25f).SetEase (Ease.OutBack).From();
			}
		}
	}

	void ChangeMenuState(){
		
		// Change the Scene State 
		GM.SetSceneState (SceneState.MENU_SELECTED);
	}
}