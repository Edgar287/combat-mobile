﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;


public class IntroUIController : MonoBehaviour {

	GameManager GM;

	Image[] splashArray;
	Sequence splashSequence;
	int indexTween = 0;

	bool allSplashEnd = false;

	void Awake () {

		// Create GameManager instance
		GM = GameManager.Instance;

		// Disable script to cotrol when calls the Start() method
		this.enabled = false;
	}


	#region before Start()

	public void ActiveUI () {

		Debug.Log ("ActiveUI");

		// Call the method to set defaults params
		SetDefaults ();
	}

	// Set the initial parameters of the objects before Start() method
	void SetDefaults () {

		// Get splash screens and add into the array
		splashArray = GetComponentsInChildren<Image>();

		// Set the splash screens to alpha 0
		float alpha = 0f;
		Color color;

		for (int i = 0; i < splashArray.Length; i++) {

			color = splashArray [i].color;
			color.a = alpha;
			splashArray [i].color = color;
		}

		// Enbale script, automatically calls the Start() method
		this.enabled = true;
	}

	#endregion


	// Use this for initialization
	void Start () {

		// Initialize with the preferences set in DOTween's Utility Panel
		DOTween.Init();
	
		// Show first splash with 1 secons of delay
		ShowSplashScreen (1.0f);
	}
	
	// Update is called once per frame
	void Update () {

		// Touching the screen ends the current splash and go to the next splash
		if (Input.GetMouseButtonDown (0) && !allSplashEnd)
			HideSplashScreen (0.0f);
	}
		
	// Show the current screen // Controls the fade & scale
	void ShowSplashScreen (float delay) {

		float timeTween = 1.5f;

		// Create a free Sequence to use
		splashSequence = DOTween.Sequence();
		// Delay the whole Sequence
		splashSequence.PrependInterval (delay)
		// Add a Scale tween at the beginning, From tween
			.Append (splashArray [indexTween].transform.DOScale (0.85f, timeTween).From ())
			// Insert a Fade tween, beggins with same delay of Sequence & has the same duration, with onComplete callback
			.Insert (delay, splashArray [indexTween].DOFade (1.0f, timeTween).OnComplete(()=>HideSplashScreen(delay)));
	}


	// Method called by touching the screen or when the tween sequence ends
	void HideSplashScreen (float delay) {

		// Check if the tween sequence is active and if so kills
		// For when the method is called by touching the screen
		if (splashSequence.IsPlaying ()) {
			splashSequence.Kill ();	
		}

		// Check if splash FadeOut is tweening
		if (!DOTween.IsTweening (splashArray [indexTween]) && !allSplashEnd) {
			// Hide the actual splash by FadeOut tweening, with onComplete callback
			splashArray [indexTween].DOFade (0.0f, 0.25f).SetDelay(delay).OnComplete (()=>NextSplashScreen(delay));
		}
	}
		
	void NextSplashScreen (float delay) {

		// Check if there are more screens to show
		if (indexTween < splashArray.Length - 1) {

			indexTween++;

			ShowSplashScreen (delay);

		} else if(!allSplashEnd){

			// All Splash Screens have ended
			allSplashEnd = true;

			DOTween.ClearCachedTweens ();

			// Change the Scene State 
			GM.SetSceneState (SceneState.INTRO_COMPLETE);
		}
	}
}
