﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

// Add all necessary states for scenes
public enum SceneState
{
	INIT,
	SAVE_DATA,
	CHANGE,
	INTRO_COMPLETE,
	MENU_SELECTED,
}

#if UNITY_EDITOR
[System.Serializable]
public class SceneStateManager {
#endif
	
#if !UNITY_EDITOR
internal class SceneStateManager {
#endif

	public SceneState sceneState {get; private set;}

	public void SetState (SceneState state)
	{		
		Debug.Log ("SetState :: " + state);

		sceneState = state;

		// Handles the state change in the GameManager
		GameManager.Instance.HandleStateChanges ();
	}
}
