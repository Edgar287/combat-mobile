﻿using UnityEngine;
using System.Collections;

internal class GameEventManager {

	// State Change Event for ScenesControllers (SceneStateManager)
	public delegate void OnStateChangeHandler ();
	public event OnStateChangeHandler OnStateChange;

	public void StateChangeEvent() {

		if (OnStateChange != null) {

			OnStateChange ();
		}
	}
}
