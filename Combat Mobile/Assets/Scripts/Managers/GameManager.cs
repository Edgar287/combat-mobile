﻿

using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {

	// Guarantee this will be always a singleton only - can't use the constructor!
	protected GameManager () {} 

	public Language language = new Language();

	internal GameSceneManager GameSceneManager;

	internal GameEventManager GameEventManager;

	#if UNITY_EDITOR
	public SceneStateManager SceneStateManager;
	#endif

	#if !UNITY_EDITOR
	internal SceneStateManager SceneStateManager;
	#endif
	
	// getter for current scene controller 
	SceneController _sceneController;


	void Awake () {

		// Create instance language
		language = CreateLanguage();

		// Create instance managers
		GameSceneManager = CreateGameSceneManager();

		GameEventManager = CreateGameEventManager ();

		SceneStateManager = CreateSceneStateManager ();

		//Add more Managers like, LoadManager, SoundManager, DataManager, ...
	}


	#region Private Methods

	// Creates and sets the language for the first time
	Language CreateLanguage (){

		// Create a new instance of the game's language
		Language lan = new Language ();

		// Set Language for the first time
		lan.SetLanguage ("es");

		return lan;
	}


	// Create the scene manager and fill necessary references
	GameSceneManager CreateGameSceneManager () {

		// Create a new instance of the game's scene manager
		GameSceneManager gameSceneManager = new GameSceneManager();

		// Create Scenes references in compiler
		#if UNITY_EDITOR
			gameSceneManager.CreateScenesReferences ();
		#endif

		// return the scene manager
		return gameSceneManager;
	}


	// Create the event manager
	GameEventManager CreateGameEventManager () {

		// Create a new instance of the game's event manager
		GameEventManager gameEventManager = new GameEventManager();

		// Return the event manager
		return gameEventManager;
	}


	// Create the scene state manager
	SceneStateManager CreateSceneStateManager () {

		// Create a new instance of the game's event manager
		SceneStateManager sceneStateManager = new SceneStateManager();

		// Return the scene state manager
		return sceneStateManager;
	}

	#endregion



	#region Public Methods

	// Dispatch the state change event in the scene
	public void HandleStateChanges () {
	
		GameEventManager.StateChangeEvent ();
	}

	public void ConfigureSceneOnAwake (GameObject sceneobject) {

		// Rename current SceneController in compiler
		#if UNITY_EDITOR
			GameSceneManager.RenameSceneController (sceneobject);
		#endif

		// Basic common configuration for all scenes 
		GameSceneManager.ConfigureCurrentScene ();

	}

	// Get & Set the current scene controller
	public SceneController sceneController {
		get {
			sceneController = GetComponentInChildren<SceneController> ();
			return _sceneController;
		}
		set {
			_sceneController = value;
		}
	}

	// Get the SceneState
	public SceneState GetSceneState () {

		return SceneStateManager.sceneState;
	}
		
	// Set and change the SceneState
	public void SetSceneState (SceneState state) {

		SceneStateManager.SetState (state);
	}

	// Get the GameScene
	public GameScene GetGameScene () {

		return GameSceneManager.scene;
	}

	// Set the GameScene // not used at the momment
	public void SetGameScene (GameScene scene) {

		GameSceneManager.SetScene (scene);
	}

	// Change the GameScene - Load Scene 
	public void LoadNextScene (GameScene scene) {

		GameSceneManager.LoadScene (scene);
	}

	// Allow runtime registration of global objects
	public T RegisterComponent<T> () where T: Component {
		
		return Instance.GetOrAddComponent<T>();
	}

	#endregion



}
	
