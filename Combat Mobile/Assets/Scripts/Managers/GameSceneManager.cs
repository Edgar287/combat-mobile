﻿using System.IO;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

// The UnityEditor namespace is not available in builds, so we exclude part of the code in build
#if UNITY_EDITOR
	using UnityEditor;
#endif


// Add all scenes of the game
public enum GameScene
{
	Intro,
	Menu,
	Combate,
	CombateTemp,
	CharacterSelection
}

internal class GameSceneManager {

	// Scenes Methods in compiler
	#if UNITY_EDITOR
		EditorBuildSettingsScene[] allScenes;
		string[] scenesNames;

		public void CreateScenesReferences () {

			// Create array references of build scenes in compiler
			allScenes = EditorBuildSettings.scenes;
			scenesNames = new string[allScenes.Length];

			string path;
			for (int i = 0; i < allScenes.Length; i++) {
				path = Path.GetFileNameWithoutExtension (allScenes [i].path);
				scenesNames[i] = path;
			//	Debug.Log ("Full Path : Scene : " + allScenes [i].path);
			//	Debug.Log ("Clear Path : Scene : " + path);
			}
		}
	#endif


	public GameScene scene {get; private set;}

	// Rename current SceneController for more visual control in compiler
	public void RenameSceneController (GameObject sceneobject) {

		string sceneName = SceneManager.GetActiveScene ().name;
		sceneobject.name = "(" + sceneName + ") " + sceneobject.name;
	}

	public void ConfigureCurrentScene () {

		// Set the GameScene enum to current scene name
		string nameScene = SceneManager.GetActiveScene ().name;
		scene = (GameScene)System.Enum.Parse (typeof(GameScene), nameScene);

		Debug.Log ("ConfigureCurrentScene :: " + scene + " :: buildIndex :: "+SceneManager.GetActiveScene ().buildIndex);

		// Set INIT as current scene state 
		GameManager.Instance.SetSceneState (SceneState.INIT);

	}

	public void SetScene(GameScene gameScene) {

		scene = gameScene;
	}

	public void LoadScene (GameScene newscene) {

		Debug.Log ("LoadNewScene :: " + newscene);

		// Force SceneState.CHANGE to remove the callback of current scene
		GameManager.Instance.SetSceneState (SceneState.CHANGE);

		scene = newscene;

		SceneManager.LoadScene (scene.ToString ());

	}

}
