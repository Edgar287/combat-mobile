﻿using UnityEngine;
using System.Collections;

public class PlayerData : MonoBehaviour {

	private float currentLife;
	public float maxLife;

	private float currentStamina;
	public float maxStamina;

	private bool isOnFloor;
	private bool isDead;
	public bool isHero; // Is the character which the user is controlling?
	public float jumpForce = 5f;
	public float movementSpeed;

	public bool airControl;


	// Use this for initialization
	void Start () {
		fullLife();
		fullStamina();
	}

	public void fullLife(){
		currentLife = maxLife;
	}

	public void fullStamina(){
		currentStamina = maxStamina;
	}

	public void reduceAmountLife(float value){
		currentLife -= value;
	}

	public void reducePerscentLife(float value){
		currentLife -= maxStamina * (value/100);
	}

	public void addStamina(float value){
		currentStamina += value;
	}

	public float CurrentLife {
		get {
			return currentLife;
		}
		set {
			currentLife = value;
		}
	}

	public float CurrentStamina {
		get {
			return currentStamina;
		}
		set {
			currentStamina = value;
		}
	}

	public bool IsHero {
		get {
			return isHero;
		}
	}

	public float JumpForce {
		get {
			return jumpForce;
		}
	}

	public float MovementSpeed {
		get {
			return movementSpeed;
		}
	}

	public bool IsDead {
		get {
			return isDead;
		}set{
			isDead = value;
		}
			
	}

	public bool IsOnFloor {
		get {
			return isOnFloor;
		}
		set {
			isOnFloor = value;
		}
	}

	public bool AirControl {
		get {
			return airControl;
		}
	}
}
